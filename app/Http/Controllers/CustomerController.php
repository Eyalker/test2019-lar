<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use App\Customer;
use App\User;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $user = User::Find($id);
        $customers = Customer::All();
        return view('customers.index', ['customers' => $customers],['createduserid' => $id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new Customer();
        $id = Auth::id(); //the idea of the current user
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->user_id = $id;
        $customer->save();
        return redirect('customers');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id = Auth::id();
        $customer = Customer::find($id);
        if (Gate::denies('manager')) {
            if($customer->user_id != $user_id)
            abort(403,"Sorry, You Don't hold the premission to edit this customer");
        }
        return view('customers.edit', compact('customer'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // if (Gate::allows('manager')) {
        //     abort(403,"Sorry, You Don't hold the premission to edit this customer..");
        //  }   
        $customer = Customer::find($id);
        $customer->update($request->except(['_token'])); 
        return redirect('customers');  
    }


    public function done($id)
    {

        if (Gate::denies('manager')) {
            abort(403,"You are not allowed to mark custer as buyer..");
         }          
        $customer = Customer::findOrFail($id);            
        $customer->status = 1; 
        $customer->save();
        return redirect('customers');    
    }    
    
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
         $customer = Customer::findOrFail($id);
         $customer->delete();
         return redirect('customers'); 
    }


}
