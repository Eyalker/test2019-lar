@extends('layouts.app')

@section('content')
<h1 style="padding-left:30px">Update Customer Details</h1>
<form style="padding-left:30px" method="post"  action="{{action('CustomerController@update', $customer->id) }}">
 @method('PATCH')
 @csrf
 <div class="form-group">
   <label for="item">Customer name:</label>
   <input type="text" class="form-control" name="name" value="{{ $customer->name}}" />
   <label for="item">Customer email:</label>
   <input type="text" class="form-control" name="email" value="{{ $customer->email}}" />
   <label for="item">Customer phone:</label>
   <input type="text" class="form-control" name="phone" value="{{ $customer->phone}}" />

 </div>
 <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection