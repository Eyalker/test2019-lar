@extends('layouts.app')

@section('content')


    <H1 style="padding-left:30px">Customers List </H1>
<a style="padding-left:50px" href="{{route('customers.create')}}">Add New Customer</a>
<ul>
        @foreach($customers as $customer)
        @if($customer->user->id==$createduserid)
        <li style="font-weight:bold">
        @else
        <li>
        @endif
            @if($customer->status)
            <span style="color:green">Name: {{$customer->name}} | Email: {{$customer->email}} | Phone: {{$customer->phone}} | Created by: {{$customer->user->name}} </span>
            @else
            <span>Name: {{$customer->name}} | Email: {{$customer->email}} | Phone: {{$customer->phone}} | Created by: {{$customer->user->name}} </span>
            @endif
            <a style="padding-left:10px" href="{{route('customers.edit', $customer->id)}}">Edit</a> 
            @can('manager')
            <a style="padding:10px" href="{{route('delete', $customer->id)}}">Delete</a> 
            @endcan
            @can('salesrep')
            Delete
            @endcan
            @cannot('salesrep')
            @if ($customer->status == 0)
            @can('manager')
             <a href="{{route('done', $customer->id)}}">Deal Closed</a>
            @endcan 
            @else
            Closed!
            @endif
            @endcannot
        </li>
        
        @endforeach
</ul>  
 @endsection